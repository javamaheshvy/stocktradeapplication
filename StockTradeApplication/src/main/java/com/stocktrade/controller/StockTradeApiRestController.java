package com.stocktrade.controller;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.stocktrade.model.Trade;
import com.stocktrade.model.User;
import com.stocktrade.repository.TradeRepository;
import com.stocktrade.repository.UserRepository;

@RestController
public class StockTradeApiRestController {

	@Autowired
	TradeRepository tradeRepository;

	@Autowired
	UserRepository userRepository;

	@GetMapping("/trades")
	public ResponseEntity getTrades() {

		ResponseEntity responseEntity = null;

		List<Trade> tradeList = tradeRepository.findAll();

		List<Trade> sortedTradeList = tradeList.stream().sorted(Comparator.comparing(Trade::getId))
				.collect(Collectors.toList());

		responseEntity = new ResponseEntity<>(sortedTradeList, HttpStatus.OK);

		return responseEntity;
	}

	@GetMapping("/trades/users/{userID}")
	public ResponseEntity getTradesByUser(@PathVariable("userID") Long userId) {

		ResponseEntity responseEntity = null;

		User user = userRepository.findById(userId);

		if (user != null) {

			List<Trade> tradeList = tradeRepository.findAll();

			List<Trade> sortedUserTradeList = tradeList.stream()
					.filter(trade ->  trade.getUser() != null && trade.getUser().getId() != null && userId != null && trade.getUser().getId().longValue() == userId.longValue())
					.sorted(Comparator.comparing(Trade::getId)).collect(Collectors.toList());

			responseEntity = new ResponseEntity<>(sortedUserTradeList, HttpStatus.OK);

		} else {
			responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return responseEntity;
	}

	@GetMapping("/trades/stocks/{stockSymbol}")
	public ResponseEntity getTradesByStockSymbol(@PathVariable("stockSymbol") String stockSymbol) {

		ResponseEntity responseEntity = null;
		List<Trade> sortedstockSymbolTradeList = null;

		if (stockSymbol != null) {

			List<Trade> tradeList = tradeRepository.findAll();

			sortedstockSymbolTradeList = tradeList.stream().filter(trade -> trade.getStockSymbol() != null && trade.getStockSymbol().equals(stockSymbol))
					.sorted(Comparator.comparing(Trade::getId)).collect(Collectors.toList());

			responseEntity = new ResponseEntity<>(sortedstockSymbolTradeList, HttpStatus.OK);

		}

		if (sortedstockSymbolTradeList == null || sortedstockSymbolTradeList.isEmpty()) {
			responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return responseEntity;
	}

	@GetMapping("/trades/users/{userID}/stocks/{stockSymbol}")
	public ResponseEntity getTradesByUserByStockSymbol(@PathVariable("userID") Long userId, @PathVariable("stockSymbol") String stockSymbol) {

		ResponseEntity responseEntity = null;

		User user = userRepository.findById(userId);
	
		List<Trade> sortedTradByUserBystockSymbolList = null;

		if (user != null && stockSymbol != null) {

			List<Trade> tradeList = tradeRepository.findAll();

			sortedTradByUserBystockSymbolList = tradeList.stream()
					.filter(trade -> trade.getUser() != null && trade.getUser().getId() != null && userId != null && trade.getUser().getId().longValue() == userId.longValue() && trade.getStockSymbol() != null && trade.getStockSymbol().equals(stockSymbol))
					.sorted(Comparator.comparing(Trade::getId)).collect(Collectors.toList());

			responseEntity = new ResponseEntity<>(sortedTradByUserBystockSymbolList, HttpStatus.OK);

		} else {
			responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return responseEntity;
	}
	
	@PostMapping(name = "/trades", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE})
	// @RequestMapping(value = "/trades", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity createTrade(@RequestBody Trade trade) {

		ResponseEntity responseEntity = null;

		if (trade != null) {

			User user = trade.getUser();

			if (user != null) {

				Long userId = user.getId();

				User userFromDB = userRepository.findById(userId);

				if (userFromDB == null) {
					userRepository.save(user);
				}

			}

			Long tradeId = trade.getId();

			Trade tradeFromDB = tradeRepository.findById(tradeId);

			if (tradeFromDB != null && tradeFromDB.getId() != null && tradeId != null
					&& tradeFromDB.getId().longValue() == tradeId.longValue()) {
				responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else {
				tradeRepository.save(trade);
				responseEntity = new ResponseEntity<>(HttpStatus.CREATED);
			}

		}

		return responseEntity;
	}

	@DeleteMapping("/erase")
	public ResponseEntity deleteTrades() {

		tradeRepository.deleteAll();

		ResponseEntity responseEntity = new ResponseEntity<>(HttpStatus.OK);

		return responseEntity;
	}

}
