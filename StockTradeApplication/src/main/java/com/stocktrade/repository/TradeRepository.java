package com.stocktrade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stocktrade.model.Trade;

public interface TradeRepository extends JpaRepository<Trade, Long>{
	
	public Trade findById(Long tradeId);
}
