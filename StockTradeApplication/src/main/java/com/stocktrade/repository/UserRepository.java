package com.stocktrade.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.stocktrade.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	public User findById(Long userId);

}
